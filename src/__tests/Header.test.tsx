import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from '../Header';

test('Rendering',  () => {
    render(<Header />);
    const logo = screen.getByAltText("Stan");
    expect(logo).toBeInTheDocument();
    const menuLabels = ["Home", "TV Shows", "Movies"];
    menuLabels.forEach(menuLabel => {
        const menuItem = screen.getByText(menuLabel);
        expect(menuItem).toBeInTheDocument();
    })
})
