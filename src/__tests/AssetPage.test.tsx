import React from 'react';
import { render, screen } from '@testing-library/react';
import AssetPage from '../AssetPage';

test("Rendering a loaded asset", () => {
    const asset: Asset = {
        id: 123,
        title: "sasha's amazing race",
        description: "This is the description of the asset",
        genre: "comedy",
        rating: "M",
        image: "https://streamcoimg-a.akamaihd.net/000/672/98/67298-PosterArt-2039396c9e27d6271c96776414d6a38c.jpg?resize=512px:*&quality=75&preferredFormat=image/jpeg",
        language: "english",
        type: "series",
        year: 1986
    }

    render(<AssetPage {...asset} loading={false} />);

    expect(screen.getByText(asset.title)).not.toBeNull();
    expect(screen.getByText(asset.description)).not.toBeNull();
    expect(screen.getByText(asset.genre)).not.toBeNull();
    expect(screen.getByText(asset.rating)).not.toBeNull();
    expect(screen.getByText(asset.language)).not.toBeNull();
    expect(screen.getByText(asset.type)).not.toBeNull();
    expect(screen.getByText(asset.year)).not.toBeNull();

    const image = (screen.getByAltText(asset.title) as HTMLImageElement);
    expect(image).not.toBeNull();
    expect(image.src).toBe(asset.image);
});
