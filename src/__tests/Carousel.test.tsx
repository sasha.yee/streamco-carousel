import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import Carousel from '../Carousel';

interface SimpleCarouselProps {
    active: boolean;
}

function SimpleCarousel(props: SimpleCarouselProps) {
    return (<div className={props.active ? 'active item' : 'item'}>Carousel Item</div>);
}

test("Rendering", () => {
    const carouselItems = [
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />,
        <SimpleCarousel active={false} />
    ];

    render(<Carousel carouselItems={carouselItems as any} />)
    const items = screen.getAllByText("Carousel Item");
    expect(items.length).toBe(6);

    expect(items[0]).toHaveClass("active");
    expect(items[1]).not.toHaveClass("active");
    expect(items[2]).not.toHaveClass("active");
    expect(items[3]).not.toHaveClass("active");

    userEvent.keyboard("{arrowright}");

    expect(items[0]).not.toHaveClass("active");
    expect(items[1]).toHaveClass("active");
    expect(items[2]).not.toHaveClass("active");
    expect(items[3]).not.toHaveClass("active");

    userEvent.keyboard("{arrowleft}");

    expect(items[0]).toHaveClass("active");
    expect(items[1]).not.toHaveClass("active");
    expect(items[2]).not.toHaveClass("active");
    expect(items[3]).not.toHaveClass("active");

    userEvent.keyboard("{arrowleft}");

    expect(items[0]).toHaveClass("active");
    expect(items[1]).not.toHaveClass("active");
    expect(items[2]).not.toHaveClass("active");
    expect(items[3]).not.toHaveClass("active");
});
