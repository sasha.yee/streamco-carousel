interface Asset {
    description: string;
    genre: string;
    id: number;
    image: string;
    language: string;
    rating: "G" | "PG" | "M" | "MA 15+" | "R 18+";
    title: string;
    type: string;
    year: number;
}

interface CarouselProps {
    active: boolean;
}

