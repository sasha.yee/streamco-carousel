import React from 'react';
import logo from './logo.svg';

import './Header.css';

function Header() {
    return (
        <header className="header">
            <ul>
                <li className="logo">
                    <img src={logo} className="App-logo" alt="Stan" />
                </li>
                <li className="active">Home</li>
                <li>TV Shows</li>
                <li>Movies</li>
            </ul>
        </header>
    );
}

export default Header;
