import { getActiveItems } from './CarouselUtil';

test("getActiveItems must return the correct values", () => {
    expect(getActiveItems(0, 6, 25)).toEqual({start: 0, end: 6});
    expect(getActiveItems(1, 6, 25)).toEqual({start: 0, end: 6});
    expect(getActiveItems(6, 6, 25)).toEqual({start: 6, end: 12});
    expect(getActiveItems(7, 6, 25)).toEqual({start: 6, end: 12});
    expect(getActiveItems(23, 6, 25)).toEqual({start: 18, end: 24});
    expect(getActiveItems(24, 6, 25)).toEqual({start: 24, end: 25});
})

test("getActiveItems must return `undefined` when the values passed in aren't acceptable", () => {
    expect(getActiveItems(-1, 10, 33)).toBe(undefined);
    expect(getActiveItems(1, -37, 29)).toBe(undefined);
    expect(getActiveItems(31, 10, -83)).toBe(undefined);
});
