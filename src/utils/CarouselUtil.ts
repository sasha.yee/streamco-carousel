export const getActiveItems = (index: number, limit: number, itemCount: number, leftPad = 0, rightPad = 0): { start: number, end: number} | undefined => {
    if(index < 0 || limit < 0 || itemCount < 0) return undefined;

    let test = 0;
    let start = 0;
    while (test <= index) {
        if (test % limit === 0) start = test;
        test++;
    }

    return {
        start: Math.max(0, start - leftPad),
        end: Math.min(start + limit + rightPad, itemCount)
    }
}
