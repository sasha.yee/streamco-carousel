import React, { ReactElement } from 'react';
import { getActiveItems } from './utils/CarouselUtil';

import './Carousel.css';

interface Props {
    loading: boolean;
    carouselItems: JSX.Element[];
    initialSelectedAsset: number | undefined;
}

interface State {
    activeIndex: number;
}

const ACTIVE_ITEM_COUNT = 5;

class Carousel extends React.Component<Props, State> {
    state = {
        activeIndex: this.props.initialSelectedAsset ? this.props.initialSelectedAsset : 0
    }

    componentDidMount() {
        if(this.props.loading === true) return;
        window.addEventListener('keyup', this.onKeyUp);
    }


    componentDidUpdate(oldProps: Props) {
        if(oldProps.loading === true && this.props.loading === false)
        window.addEventListener('keyup', this.onKeyUp);
    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.onKeyUp);
    }

    private onKeyUp = (e: KeyboardEvent) => {
        e.preventDefault();

        if(e.code === 'ArrowLeft') this.prevIndex();
        if(e.code === 'ArrowRight') this.nextIndex();
    }

    private prevIndex() {
        const prevIndex = Math.max(0, this.state.activeIndex - 1);
        this.setState({ activeIndex: prevIndex }) ;
    }

    private nextIndex() {
        const nextIndex = Math.min(this.props.carouselItems.length - 1, this.state.activeIndex + 1);
        this.setState({ activeIndex: nextIndex }) ;
    }

    private getActiveItems() {
        const { carouselItems } = this.props;
        if(carouselItems.length === 0) return [];

        const activeItems = getActiveItems(this.state.activeIndex, ACTIVE_ITEM_COUNT, this.props.carouselItems.length, 0, 1);
        if(activeItems === undefined) return [];
        return carouselItems.slice(activeItems.start, activeItems.end);
    }

    render() {
        return (
            <div className="carousel">
                <ul className="carousel-stage">
                    {this.getActiveItems().map((item, index) => (
                        <li key={`carousel-item-${index}`}>
                            {React.cloneElement(item as unknown as ReactElement, { active: this.state.activeIndex % ACTIVE_ITEM_COUNT === index })}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }

}

export default Carousel;
