import React from 'react';
import './AssetPage.css';

interface AssetPageProps extends Asset {
    loading: boolean;
}

export default function AssetPage(props: AssetPageProps): JSX.Element {
    const classes = ['asset'];
    let image = (<img src={props.image} alt={props.title} className="asset-poster" />);

    if(props.loading === true) {
        classes.push('loading')
        image = <div className="poster-placeholder" />
    }

    return (
        <div className={classes.join(' ')}>
            {image}

            <div className="asset-details">
                <h1>{props.title}</h1>
                <p>
                    <span>{props.rating}</span> |
                    <span>{props.year}</span> |
                    <span>{props.type}</span> |
                    <span>{props.genre}</span> |
                    <span>{props.language}</span>
                </p>

                <p className="description">{props.description}</p>
            </div>
        </div>
    );
}
