import React from 'react';

import './Poster.css';

interface Props extends Asset {
    active: boolean;
    onSelect: (id: Asset["id"]) => void;
}

class Poster extends React.Component<Props, unknown> {
    componentDidMount() {
        window.addEventListener('keyup', this.onKeyUp);
    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.onKeyUp);
    }

    private onKeyUp = (e: KeyboardEvent) => {
        e.preventDefault();
        if(this.props.active === false) return;
        if(e.code === 'Enter') this.props.onSelect(this.props.id);
    }

    render() {
        const classes = ['poster'];
        if(this.props.active === true)  {
            classes.push('active');
        }


        return (
            <div className={classes.join(" ")}>
                <a href={`/${this.props.id}`}>
                    <img src={this.props.image} alt={this.props.title} />
                </a>
            </div>
        )
    }
}

export default Poster;
