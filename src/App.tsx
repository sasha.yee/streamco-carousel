import React from 'react';
import Header from './Header';
//import CarouselComponent from "./CarouselComponent";
import Carousel from './Carousel';
import Poster from './Poster';
import AssetPage from './AssetPage';
import PlaceholderPoster from './PlaceholderPoster';

import './App.css';

interface AppState {
    loading: boolean;
    data: Asset[];
    carouselItems: JSX.Element[];// CarouselComponent<CarouselProps, unknown>[];
    lastSelected: number | undefined;
    selectedAsset: Asset | undefined;
    error: string | undefined;
}

const DATA_ENDPOINT = 'https://raw.githubusercontent.com/StreamCo/tv-coding-challenge/master/data.json';

const loadingAsset: Asset = {
    "id": 0,
    "title": "Dr. Death",
    "description": "Dr. Death tells the terrifying true story of Dr. Christopher Duntsch (Joshua Jackson), a brilliant but sociopathic neurosurgeon whose patients leave his operating room either permanently maimed or dead, and the two doctors who set out to stop him.",
    "type": "series",
    "image": "",
    "rating": "MA 15+",
    "genre": "Drama",
    "year": 2021,
    "language": "English"
}

class App extends React.Component<unknown, AppState> {
    state = {
        loading: true,
        data: [],
        carouselItems: [
            <PlaceholderPoster />,
            <PlaceholderPoster />,
            <PlaceholderPoster />,
            <PlaceholderPoster />,
            <PlaceholderPoster />,
            <PlaceholderPoster />,
        ],
        lastSelected: 0,
        selectedAsset: undefined,
        error: undefined
    }

    componentDidMount() {
        if(window.location.pathname !== '/') {
            this.setState({ selectedAsset: loadingAsset })
        }

        this.loadData();

        window.addEventListener('keyup', this.onKeyUp);
    }

    async loadData() {
        const loading = false;
        const request = await fetch(DATA_ENDPOINT);
        try {
            if(request.ok === false) throw new Error('An unknown error occurred. Please try again later');
            const data: Asset[] = await request.json();

            const carouselItems = data.map((asset, key) => (<Poster {...asset} active={false} key={`poster-${key}`} onSelect={this.setAsset} />))

            let selectedAsset = undefined;
            let lastSelected = undefined;
            if(this.state.selectedAsset !== undefined) {
                const selectedAssetId = parseInt(window.location.pathname.replace('/', ''));
                lastSelected = data.findIndex(asset => asset.id === selectedAssetId);
                selectedAsset = data.find(asset => asset.id === selectedAssetId);

                if(selectedAsset === undefined) throw new Error('An unknown error occurred. Please try again later');
            }

            this.setState({ data, carouselItems, loading, selectedAsset, lastSelected });
        } catch (e: unknown) {
            const error = (e instanceof Error) ? e.message : 'Oops';

            this.setState({error, loading });
        }

    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.onKeyUp);
    }

    private onKeyUp = (e: KeyboardEvent) => {
        if(e.code === 'Backspace') this.setAsset(undefined);
    }

    setAsset = (selectedId: Asset["id"] | undefined) => {
        let selectedAsset: any; // Something funky is happening here. I don't like using any
        let lastSelectedId = this.state.lastSelected;
        let path = "/";

        if(selectedId !== undefined) {
            selectedAsset = this.state.data.find((asset: Asset) => asset.id === selectedId);
            lastSelectedId = this.state.data.findIndex((asset: Asset) => asset.id === selectedId);

            if(selectedAsset && 'id' in selectedAsset) {
                path = `/${selectedAsset.id}`;
            }
        }

        this.setState({ selectedAsset, lastSelected: lastSelectedId });
        window.history.pushState({}, "", path);
    }

    render() {
        return (
            <div className="App">
                <Header />
                {this.renderContents()}
            </div>
        );
    }

    renderContents() {
        if(this.state.loading === false && this.state.error !== undefined) {
            return (<div className="error">{this.state.error}</div>);
        }

        if(this.state.selectedAsset !== undefined)  {
            return (<AssetPage {...this.state.selectedAsset} loading={this.state.loading} />);
        }

        return (<Carousel
                    carouselItems={this.state.carouselItems}
                    initialSelectedAsset={this.state.lastSelected}
                    loading={this.state.loading}
        />);
    }
}

export default App;
