# Getting Started with Create React App

This project was used Create React App as a starting point, as it's easy to get up and running without wading too deeply into WebPack.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `dist` folder.\

The build is minified and with these files the app is ready to be deployed.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# Decision making process
I started with Create React App as I wanted to get started straight away and didn't want to have to worry about getting bogged down by configuration before even starting.

Regarding the Carousel I wanted to make sure that it encapsulated as much of the functionality as possible to ensure that it was maintainable in the future. I'm happy that it doesn't matter what is passed into the carousel and that it _just works_.

# Other things to do given more time


1. Add an automatic formatter such as eslint or prettier to take away the inconsistencies in code and prevent personal formatting preferences becoming a point of debate within the team.
1. More tests for the carousel covering more interaction with it.
1. Tests for carousel to ensure the order is correct and making sure that the right items are present in the DOM.
1. Types for the components being passed into the Carousel should be better formed to ensure that the necessary props are available.
1. Right now the carousel relies on the assets all being the same size and that they will automatically fit within the viewport. To make it more flexible the carousel should determine what can be displayed within the viewport.
1. Key bindings should only exist in one place. I'd use a context as a means of adding event handlers in one place and move callbacks for the events into the components where the actions are relevant. These components would use an abstract class to ensure that the required functions were present.
1. Figure out why 1.js is being created in the dist folder and implement a better way of preventing it from existing in the dist folder.
1. There aren't any tests for the error paths. This should be covered.
1. Make the Poster component more general so that it could be used in both the carousel and the AssetPage component.
